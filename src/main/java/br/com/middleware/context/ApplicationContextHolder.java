package br.com.middleware.context;

public class ApplicationContextHolder {

    private static final ThreadLocal<ApplicationContext> CONTEXT_THREAD_LOCAL = new ThreadLocal();

    public ApplicationContextHolder() {
    }

    public static ThreadLocal<ApplicationContext> getContextHolder() {
        return CONTEXT_THREAD_LOCAL;
    }

    public static ApplicationContext getContext() {
        ApplicationContext applicationContext = (ApplicationContext)CONTEXT_THREAD_LOCAL.get();
        if (applicationContext == null) {
            applicationContext = new ApplicationContext();
            CONTEXT_THREAD_LOCAL.set(applicationContext);
        }

        return applicationContext;
    }

    public static void resetContext() {
        CONTEXT_THREAD_LOCAL.remove();
    }

    public static boolean isEmpty() {
        return CONTEXT_THREAD_LOCAL.get() == null;
    }

}
