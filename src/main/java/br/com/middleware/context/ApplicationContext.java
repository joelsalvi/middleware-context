package br.com.middleware.context;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ApplicationContext {

    private String localTrackingId;
    private String globalTrackingId;
    private String contextTrackingId;
    private String organization;
    private String application;
    private String apiSlug;
    private String channel;
    private Map<String, Object> custom;

    public String getGlobalTrackingId() {
        if (this.globalTrackingId == null || this.globalTrackingId.trim().isEmpty()) {
            this.globalTrackingId = UUID.randomUUID().toString().replaceAll("-", "");
        }

        return this.globalTrackingId;
    }

    public String getLocalTrackingId() {
        if (this.localTrackingId == null || this.localTrackingId.trim().isEmpty()) {
            this.localTrackingId = UUID.randomUUID().toString().replaceAll("-", "");
        }

        return this.localTrackingId;
    }

    public Map<String, Object> getCustom() {
        if (this.custom == null) {
            this.custom = new HashMap();
        }

        return this.custom;
    }

    public void clearCustom() {
        if (this.custom != null) {
            this.custom.clear();
        }

    }

    public String getContextTrackingId() {
        return this.contextTrackingId;
    }

    public String getOrganization() {
        return this.organization;
    }

    public String getApplication() {
        return this.application;
    }

    public String getApiSlug() {
        return this.apiSlug;
    }

    public String getChannel() {
        return this.channel;
    }

    public void setLocalTrackingId(String localTrackingId) {
        this.localTrackingId = localTrackingId;
    }

    public void setGlobalTrackingId(String globalTrackingId) {
        this.globalTrackingId = globalTrackingId;
    }

    public void setContextTrackingId(String contextTrackingId) {
        this.contextTrackingId = contextTrackingId;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public void setApiSlug(String apiSlug) {
        this.apiSlug = apiSlug;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public void setCustom(Map<String, Object> custom) {
        this.custom = custom;
    }


}
